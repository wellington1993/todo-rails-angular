angular.module 'client'
  .controller 'TasksController', ($scope,Restangular) ->
    base_tasks = Restangular.all 'tasks'
    $scope.title = 'Tasks'
    $scope.new_task = 
      description: ''

    base_tasks.getList().then (response) ->
      $scope.tasks = response

    $scope.save = -> 
      base_tasks.post($scope.new_task).then (response) ->
        $scope.tasks.push response 
        $scope.new_task.description = ''

    $scope.remove = (task) ->
      task.remove().then -> 
        _.remove $scope.tasks, (p) -> p.id == task.id

    $scope.update = ->
      $scope.current_task.save().then (response) ->
        
    $scope.setCurrent = (task) -> 
      $scope.current_task = task       
